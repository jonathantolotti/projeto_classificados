<?php
require 'config.php';
require 'classes/anuncios.class.php';
if(empty($_SESSION['userLogado'])){
	header("Location: login");
}

$anuncio = new Anuncios();
if(isset($_GET['id']) && !empty($_GET['id'])){
	$id = $_GET['id'];
	$id_anuncio = $anuncio->excluirFoto($id);
}
if(isset($id_anuncio)) {
	header("Location: editar-anuncio.php?id=".$id_anuncio);
} else {
	header("Location: meus-anuncios.php");
}