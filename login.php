<?php
require 'pages/header.php';
?>

<body>
	<div class="container">
		<h1 class="display-4">Acessar:</h1>
		<?php 
		require 'classes/usuarios.class.php';
		$usuario = new Usuarios();

		if(isset($_POST['email']) && !empty($_POST['email'])){	
			$email = addslashes($_POST['email']);
			$senha = md5($_POST['senha']);
		

		if($usuario->loginUsuario($email, $senha)){
		?>
			<script type="text/javascript">window.location.href="./";</script>
		<?php
		}else{
			?>
			<div class="alert alert-danger" role="alert">
				Usuário ou senha inválidos!
			</div>
			<?php
		}
	}
		?>
		<form method="POST">
			<div class="form-group">
				<div class="form-group col-md-4">
					<label for="email">E-mail:</label>
					<input type="email" class="form-control" placeholder="Digite o e-mail" name="email" id="email">
				</div>
				<div class="form-group col-md-4">
					<label for="senha">Senha:</label>
					<input type="password" class="form-control" name="senha" id="senha">
				</div>
				<input class="btn btn-info" type="submit" value="Acessar">
			</form>
		</div>
	</body>

	<?php require 'pages/footer.php'; ?>