<?php 
class Categorias{

	public function getCategorias(){
		$categorias = array();
		global $pdo;

		$sql = $pdo->prepare("SELECT * FROM categorias");
		$sql->execute();

		if($sql->rowCount() > 0){
			$categorias = $sql->fetchAll();
		}
		return $categorias;
	}

}

?>