<?php 
class Anuncios{

	public function getTotalAnuncios(){
		global $pdo;

		$sql = $pdo->query("SELECT count(*) as c FROM anuncios");
		$row = $sql->fetch();

		return $row['c'];

	}

	public function getUltimosAnuncios($page, $perPage, $filtros){
		global $pdo;
		$offset = ($page -1)*$perPage;
		$anuncios = array();

		$filtrosring = array('1=1');

		if(!empty($filtros['categoria'])){
			$filtrosring[] = 'anuncios.id_categoria = :id_categoria';
		}
		if(!empty($filtros['preco1']) && !empty($filtros['preco2'])){
			$filtrosring[] = 'anuncios.valor BETWEEN :preco1 AND :preco2';
		}
		if(!empty($filtros['estado'])){
			$filtrosring[] = 'anuncios.estado = :estado';
		}

		$sql = $pdo->prepare("SELECT *, (select anuncios_imagens.url from anuncios_imagens where anuncios_imagens.id_anuncio = anuncios.id limit 1) as url,
			(select categorias.nome from categorias where categorias.id = anuncios.id_categoria) as categoria
			FROM anuncios WHERE ".implode(' AND ', $filtrosring)." ORDER BY id DESC limit $offset,$perPage");

		if(!empty($filtros['categoria'])){
			$sql->bindValue(":id_categoria", $filtros['categoria']);
		}
		if(!empty($filtros['preco1']) && !empty($filtros['preco2'])){
			$sql->bindValue(":preco1", $filtros['preco1']);
			$sql->bindValue(":preco2", $filtros['preco2']);
		}
		if(!empty($filtros['estado'])){
		$sql->bindValue(":estado", $filtros['estado']);
		}

	$sql->execute();

	if($sql->rowCount() > 0){
		$anuncios = $sql->fetchAll();
	}
	return $anuncios;
}

public function getMeusAnuncios(){
	global $pdo;
	$anuncios = array();
	$sql = $pdo->prepare("SELECT *, (select anuncios_imagens.url from anuncios_imagens where anuncios_imagens.id_anuncio = anuncios.id limit 1) as url FROM anuncios WHERE id_usuario = :id_usuario");
	$sql->bindValue(":id_usuario", $_SESSION['userLogado']);
	$sql->execute();

	if($sql->rowCount() > 0){
		$anuncios = $sql->fetchAll();
	}
	return $anuncios;

}

public function getAnuncio($id){
	global $pdo;
	$anuncio = array();

	$sql = $pdo->prepare("SELECT *,
		(select categorias.nome from categorias where categorias.id = anuncios.id_categoria) as categorias,
		(select usuarios.telefone from usuarios where usuarios.id = anuncios.id_usuario) as telefone
		FROM anuncios WHERE id = :id");
	$sql->bindValue(":id", $id);
	$sql->execute();
	if($sql->rowCount() > 0){
		$anuncio = $sql->fetch();
		$anuncio['fotos'] = array();
		$sql = $pdo->prepare("SELECT id, url FROM anuncios_imagens WHERE id_anuncio = :id_anuncio");
		$sql->bindValue(":id_anuncio", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$anuncio['fotos'] = $sql->fetchAll();
		}
	}
	return $anuncio;

}

public function addAnuncio($titulo, $categoria, $valor, $descricao, $estado){
	global $pdo;
	$sql = $pdo->prepare("INSERT INTO anuncios SET 
		titulo = :titulo, 
		id_categoria = :id_categoria, 
		id_usuario = :id_usuario, 
		descricao = :descricao, 
		valor = :valor, 
		estado = :estado, 
		data_cad = NOW()");
	$sql->bindValue(":titulo", $titulo);
	$sql->bindValue(":id_categoria", $categoria);
	$sql->bindValue(":id_usuario", $_SESSION['userLogado']);
	$sql->bindValue(":descricao", $descricao);
	$sql->bindValue(":valor", $valor);
	$sql->bindValue(":estado", $estado);
	$sql->execute();
}

public function editAnuncio($titulo, $categoria, $valor, $descricao, $estado, $fotos,$id){
	global $pdo;
	$sql = $pdo->prepare("UPDATE anuncios SET 
		titulo = :titulo, 
		id_categoria = :id_categoria, 
		descricao = :descricao, 
		valor = :valor, 
		estado = :estado WHERE id = :id AND id_usuario = :id_usuario");
	$sql->bindValue(":titulo", $titulo);
	$sql->bindValue(":id_categoria", $categoria);
	$sql->bindValue(":id_usuario", $_SESSION['userLogado']);
	$sql->bindValue(":descricao", $descricao);
	$sql->bindValue(":valor", $valor);
	$sql->bindValue(":estado", $estado);
	$sql->bindValue(":id", $id);
	$sql->execute();

	if(count($fotos) > 0){
		for($i=0;$i<count($fotos['tmp_name']);$i++){
			$tipo = $fotos['type'][$i];
			if(in_array($tipo, array('image/jpeg', 'image/png'))){
				$tmpName = md5(time().rand(0,999999)).'.jpg';
				move_uploaded_file($fotos['tmp_name'][$i],'assets/_images/_anuncios/'.$tmpName);

				list($width_orig, $height_orig) = getimagesize('assets/_images/_anuncios/'.$tmpName);
				$ratio = $width_orig / $height_orig;

				$width = 500;
				$height = 500;

				if($width/$height > $ratio){
					$width = $height*$ratio;
				}else{
					$height = $width/$ratio;
				}

				$img = imagecreatetruecolor($width, $height);
				if($tipo == 'image/jpeg'){
					$origi = imagecreatefromjpeg('assets/_images/_anuncios/'.$tmpName);
				}elseif($tipo == 'image/png'){
					$origi = imagecreatefrompng('assets/_images/_anuncios/'.$tmpName);
				}

				imagecopyresampled($img, $origi, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				imagejpeg($img, 'assets/_images/_anuncios/'.$tmpName, 80);


				$sql = $pdo->prepare("INSERT INTO anuncios_imagens SET 
					id_anuncio = :id_anuncio, 
					url = :url, 
					id_usuario = :id_usuario");

				$sql->bindValue(":id_anuncio", $id);
				$sql->bindValue(":url", $tmpName);
				$sql->bindValue(":id_usuario", $_SESSION['userLogado']);
				$sql->execute();
			}
		}
	}
}

public function excluirAnuncio($id){
	global $pdo;

	$sql = $pdo->prepare("DELETE FROM anuncios_imagens WHERE id_anuncio = :id_anuncio AND id_usuario = :id_usuario");
	$sql->bindValue(":id_anuncio", $id);
	$sql->bindValue(":id_usuario", $_SESSION['userLogado']);
	$sql->execute();

	$sql = $pdo->prepare("DELETE FROM anuncios WHERE id = :id_anuncio AND id_usuario = :id_usuario");
	$sql->bindValue(":id_anuncio", $id);
	$sql->bindValue(":id_usuario", $_SESSION['userLogado']);
	$sql->execute();
}

public function excluirFoto($id){
	global $pdo;
	$id_anuncio = 0;

	$sql = $pdo->prepare("SELECT id_anuncio FROM anuncios_imagens WHERE id = :id");
	$sql->bindValue(":id", $id);
	$sql->execute();

	if($sql->rowCount() > 0) {
		$row = $sql->fetch();
		$id_anuncio = $row['id_anuncio'];
	}

	$sql = $pdo->prepare("DELETE FROM anuncios_imagens WHERE id = :id AND id_usuario = :id_usuario");
	$sql->bindValue(":id", $id);
	$sql->bindValue(":id_usuario", $_SESSION['userLogado']);
	$sql->execute();

	return $id_anuncio;

}

}

?>