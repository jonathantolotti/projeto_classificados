# Classificados v1:

Sistema de classificados desenvolvido de forma colaborativa com objetivo de apredizado.

## Tecnologias utilizadas:
Utilizado POO, PHP, MySQL, JavaScript, jQuery e Bootstrap 4.1

### Para contribuir:
Caso queira contribuir, realizar um clone do repositório master e criar um branch seguindo o modelo abaixo:
tipo-breve escopo

## Tipos: 
* fea - feature;
* fix - fix;
* ref - refactory.

```
Exemplo: fix-loginEmail
```

Antes de submeter um pull request, certifique-se de utilizar o tipo correto e de escrever uma breve descrição da modificação.



## Features atuais:
* Template base;
* Cadastro de usuários;
* Login de usuários;
* Cadastro de anúncio;

## Features em desenvolvimento:
* Exibição de total de anúncios;
* Criação de sistema de buscas.

## Features ainda em backlog:
* Criação de contador de visitas nos anúncios;
* Criação de sistema de ordenamento de anúncios;
* Criação de edição do perfil de usuário;
* Criação de aprovação de anúncios (com tela de meus anúncios separando aprovado/em análise).

## Contribuições:
* **Jonathan Tolotti** - *Inicio do projeto*
