<?php 
require 'pages/header.php';
require 'classes/categorias.class.php';
require 'classes/anuncios.class.php';
?>
<?php 
if(empty($_SESSION['userLogado'])){
	?>
	<script type="text/javascript">window.location.href="login.php"</script>
	<?php
	exit;
}
$anuncio = new Anuncios();

if(isset($_POST['titulo']) && !empty($_POST['titulo'])) {
	$titulo = addslashes($_POST['titulo']);
	$categoria = addslashes($_POST['categoria']);
	$valor = addslashes($_POST['valor']);
	$descricao = addslashes($_POST['descricao']);
	$estado = addslashes($_POST['estado']);

	$anuncio->addAnuncio($titulo, $categoria, $valor, $descricao, $estado);
	header("Location: editar-anuncio");
}


?>
<div class="container">
	<h1 class="display-4">Novo anúncio:</h1>
	<div class="alert alert-info" role="alert">
		As imagens deverão ser adicionadas na edição do anúncio.
	</div>
	<form method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<div class="form-group col-md-4">
				<label for="categoria">Categoria:</label>
				<select class="form-control" name="categoria" id="categoria">
					<option></option>
					<?php 
					$categoria = new Categorias();
					$categorias = $categoria->getCategorias();
					foreach($categorias as $cat):
						?>
						<option value="<?php echo $cat['id'];?>"><?php echo $cat['nome'];?></option>
					<?php endforeach; ?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="titulo">Título:</label>
				<input type="text" class="form-control" placeholder="Digite o título" name="titulo" id="titulo">
			</div>

			<div class="form-group col-md-4">
				<label for="valor">Valor:</label>
				<input type="number" class="form-control" name="valor" id="valor">
			</div>

			<div class="form-group col-md-4">
				<label for="descricao">Descrição:</label>
				<textarea class="form-control" name="descricao" id="descricao"></textarea>
			</div>

			<div class="form-group col-md-4">
				<label for="estado">Estado de conservação:</label>
				<select class="form-control" name="estado" id="estado">
					<option value=""></option>
					<option value="USADO">Usado</option>
					<option value="NOVO">Novo</option>
				</select>
			</div>
			<input class="btn btn-info" type="submit" value="Criar">
		</form>
	</div>


</div>


<?php require 'pages/footer.php'; ?>