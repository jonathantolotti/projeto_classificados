<?php
session_start();  
/**
* Configuração de conexão ao MySQL.
*
* Arquivo de configuração de conexão ao banco de dados.
*
* @package projeto_classificados
*
* @author Jonathan T.
*
* @param $dbname - Nome da tabela
* @param $server - IP do servidor
* @param $dbuser - Usuário DB
* @param $dbpass - Senha DB
* @param $charset - colação de caracteres
* @param $dsn - Monta string de conexão
* @param $pdo - Realiza a conexão
* @param $e - Captura o erro de conexão vindo do PDOException

**/ 

	$dbname = "classificados";
	$server = "localhost";
	$dbuser = "root";
	$dbpass = "Adminuser2@172";
	$charset = "utf8";
	$dsn = "mysql:dbname=".$dbname.";"."host=".$server.";"."charset=".$charset.";";
	global $pdo;

try{

	$pdo = new PDO($dsn, $dbuser, $dbpass);

}catch(PDOException $e){
	echo "ERRO:".$e->getMessage();
	exit;
}

?>