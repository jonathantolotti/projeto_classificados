<?php require 'pages/header.php'; ?>
<?php 
if(empty($_SESSION['userLogado'])){
	?>
	<script type="text/javascript">window.location.href="login.php"</script>
	<?php
	exit;
}

?>
<div class="container">
	<h1 class="display-4">Meus anúncios:</h1>
	<a href="add-anuncio.php" class="btn btn-info">Novo anúncio</a>
	<div class="table-responsive" style="margin-top: 10px;">
		<table class="table table-hover">
			<thead class="thead-dark">
				<tr>
					<th scope="col" class="text-center">Foto</th>
					<th scope="col" class="text-center">Título</th>
					<th scope="col" class="text-center">Valor</th>
					<th scope="col" class="text-center">Estado</th>
					<th scope="col" class="text-center">Ações</th>
				</tr>
			</thead>

			<tbody>
				<?php 
				require 'classes/anuncios.class.php';
				$anuncio = new Anuncios();
				$anuncios = $anuncio->getMeusAnuncios();

				foreach($anuncios as $anuncioItem):
					?>
					<tr>
						<td class="text-center">
							<?php if(!empty($anuncioItem['url'])): ?>
								<img src="assets/_images/_anuncios/<?php echo $anuncioItem['url']; ?>" border="0" width="50px" height="50px">
								<?php else: ?>
									<img src="assets/_images/_anuncios/default.png" border="0" width="50px" height="50px" alt="Sem imagem disponível">
								<?php endif; ?>
							</td>
							<td class="text-center"><?php echo $anuncioItem['titulo'];?></td>
							<td class="text-center">R$ <?php echo number_format($anuncioItem['valor'],2,',','.'); ?></td>
							<td class="text-center"><?php echo $anuncioItem['estado'];?></td>
							<td class="text-center">
								<a href="editar-anuncio?id=<?php echo $anuncioItem['id']; ?>"><button type="button" class="btn btn-primary btn-sm">Editar</button></a>
								<a href="excluir-anuncio?id=<?php echo $anuncioItem['id']; ?>"><button type="button" class="btn btn-danger btn-sm">Excluir</button></a>
							</td>
						</tr>
					<?php endforeach; ?>	
				</tbody>
			</div>
			<?php require 'pages/footer.php'; ?>