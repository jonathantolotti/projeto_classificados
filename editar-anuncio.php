<?php 
require 'pages/header.php';
require 'classes/categorias.class.php';
require 'classes/anuncios.class.php';
?>
<?php 
if(empty($_SESSION['userLogado'])){
	?>
	<script type="text/javascript">window.location.href="login.php"</script>
	<?php
	exit;
}
$anuncio = new Anuncios();

if(isset($_POST['titulo']) && !empty($_POST['titulo'])) {
	$titulo = addslashes($_POST['titulo']);
	$categoria = addslashes($_POST['categoria']);
	$valor = addslashes($_POST['valor']);
	$descricao = addslashes($_POST['descricao']);
	$estado = addslashes($_POST['estado']);
	if(isset($_FILES['fotos'])){
	$fotos = $_FILES['fotos'];
	}else{
		$fotos = array();
	}
	$anuncio->editAnuncio($titulo, $categoria, $valor, $descricao, $estado, $fotos,$_GET['id']);
	
	?>
	<div class="alert alert-success">
		Anúncio alterado com sucesso!
	</div>
	<?php
}
if(isset($_GET['id']) && !empty($_GET['id'])){
	$info = $anuncio->getAnuncio($_GET['id']);
}else{
	?>
	<script type="text/javascript">window.location.href="meus-anuncios.php"</script>
	<?php
	exit;
}

?>
<div class="container">
	<h1 class="display-4">Editar anúncio Nº: <?php echo $_GET['id'];?></h1>
	<form method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<div class="form-group col-md-4">
				<label for="categoria">Categoria:</label>
				<select class="form-control" name="categoria" id="categoria">
					<option></option>
					<?php 
					$categoria = new Categorias();
					$categorias = $categoria->getCategorias();
					foreach($categorias as $cat):
						?>
						<option value="<?php echo $cat['id']; ?>" <?php echo ($info['id_categoria']==$cat['id'])?'selected="selected"':''; ?>><?php echo $cat['nome']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="titulo">Título:</label>
				<input type="text" name="titulo" id="titulo" class="form-control" value="<?php echo $info['titulo']; ?>" />
			</div>

			<div class="form-group col-md-4">
				<label for="valor">Valor:</label>
				<input type="number" class="form-control" name="valor" id="valor" value="<?php echo $info['valor'];?>">
			</div>

			<div class="form-group col-md-4">
				<label for="descricao">Descrição:</label>
				<textarea class="form-control" name="descricao" id="descricao"><?php echo $info['descricao'];?></textarea>
			</div>

			<div class="form-group col-md-4">
				<label for="estado">Estado de conservação:</label>
				<select class="form-control" name="estado" id="estado">
					<option value=""></option>
					<option value="USADO" <?php echo ($info['estado'] == 'USADO')?'selected="selected"':'';?>>Usado</option>
					<option value="NOVO" <?php echo ($info['estado'] == 'NOVO')?'selected="selected"':'';?>>Novo</option>
				</select>
			</div>
			<div class="form-group col-md-4">
				<label for="add_foto">Imagens do anúncio:</label>
				<input type="file" class="form-control-file" name="fotos[]" multiple>
			</div>
			<div class="card"	>
				<div class="card-header">
					Imagens:
				</div>
				<div class="card-body">
					<?php foreach($info['fotos'] as $foto): ?>
					<div class="foto_item">
						<img src="assets/_images/_anuncios/<?php echo $foto['url']; ?>" class="img-thumbnail" border="0" /><br/>
						<a href="excluir-foto.php?id=<?php echo $foto['id']; ?>" class="btn btn-danger btn-sm btn-block">Excluir</a>
					</div>

					<?php endforeach; ?>
					</div>
				</div><br>
			<input class="btn btn-info" type="submit" value="Salvar">
		</form>
	</div>


</div>


<?php require 'pages/footer.php'; ?>