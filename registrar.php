<?php
require 'pages/header.php';
?>

<body>
	<div class="container">
		<h1 class="display-4">Cadastre-se:</h1>
		<?php 
		require 'classes/usuarios.class.php';
		$usuario = new Usuarios();

		if(isset($_POST['nome']) && !empty($_POST['nome'])){
			$nome = addslashes($_POST['nome']);
			$email = addslashes($_POST['email']);
			$senha = md5($_POST['senha']);
			$telefone = addslashes($_POST['telefone']);
			if(!empty($nome) && !empty($email) && !empty($senha)){
				if($usuario->novoUsuario($nome, $email, $senha, $telefone)){
					?>
					<div class="alert alert-success" role="alert">
						Parabéns, usuário cadastrado com sucesso! <a href="login" class="alert-link">Clique aqui</a> para fazer login.
					</div>
					<?php
				}else{
					?>
					<div class="alert alert-danger" role="alert">
						Este usuário já existe! <a href="login" class="alert-link">Clique aqui</a> para fazer login.
					</div>
					<?php
				}
			}else{
				?>
				<div class="alert alert-warning" role="alert" style="width: 400px;">
					Preencha todos os campos!
				</div>
				<?php
			}
			
		}

		?>
		<form method="POST">
			<div class="form-group">
				<div class="form-group col-md-4">
					<label for="nome">Nome:</label>
					<input type="text" class="form-control" placeholder="Digite o nome completo" name="nome" id="nome">
				</div>

				<div class="form-group col-md-4">
					<label for="email">E-mail:</label>
					<input type="email" class="form-control" placeholder="Digite o e-mail" name="email" id="email">
				</div>

				<div class="form-group col-md-4">
					<label for="senha">Senha:</label>
					<input type="password" class="form-control" name="senha" id="senha">
				</div>

				<div class="form-group col-md-4">
					<label for="telefone">Telefone:</label>
					<input type="text" class="form-control" name="telefone" id="telefone">
				</div>
				<input class="btn btn-info" type="submit" value="Registrar">
			</form>
		</div>
	</body>

	<?php require 'pages/footer.php'; ?>