<?php
require 'config.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Classificados - </title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="assets/_css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/_css/style.css">
	<script type="text/javascript" src="assets/_js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="assets/_js/bootstrap.bundle.min.js"></script>
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="#">Classificados</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="index">Home</a>
			</li>
			<?php if(isset($_SESSION['userLogado']) && !empty($_SESSION['userLogado'])): ?>
			<li class="nav-item">
				<a class="nav-link" href="meus-anuncios">Meus anúncios</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="logout">Sair</a>
			</li>
			<?php else: ?>
				<li class="nav-item">
					<a class="nav-link" href="registrar">Cadastre-se</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="login">Entrar</a>
				</li>
			<?php endif; ?>
		</ul>
	</div>
</nav> 
</html>