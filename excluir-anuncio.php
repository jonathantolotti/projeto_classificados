<?php
require 'config.php';
require 'classes/anuncios.class.php';
if(empty($_SESSION['userLogado'])){
	header("Location: login");
}

$anuncio = new Anuncios();
if(isset($_GET['id']) && !empty($_GET['id'])){
	$id = $_GET['id'];
	$anuncio->excluirAnuncio($id);
}
header("Location: meus-anuncios");

?>