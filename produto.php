<?php
require 'pages/header.php'; 
require 'classes/anuncios.class.php';
require 'classes/usuarios.class.php';

$a = new Anuncios();
$u = new Usuarios();

if(isset($_GET['id']) && !empty($_GET['id'])){
	$id = addslashes($_GET['id']);

}else{
	?>
	<script type="text/javascript">window.location.href="index.php"</script>
	<?php
	exit;
}
$info = $a->getAnuncio($id);
?>
<!DOCTYPE html>
<html>
<body>
	<div class="container-fluid" style="margin-top: 5px;">
		<div class="row">
			<div class="col-sm-5">
				<div class="carousel slide" data-ride="carousel" id="carouselProduto">
					<div class="carousel-inner" role="listbox">
						<?php foreach ($info['fotos'] as $chave => $foto): ?>
							<div class="carousel-item <?php echo($chave=='0')?'active':''; ?>">
								<img src="assets/_images/_anuncios/<?php echo $foto['url']; ?>" width="600" height="500">
							</div>
						<?php endforeach; ?>
					</div>
					<a class="carousel-control-prev" href="#carouselProduto" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					</a>
					<a class="carousel-control-next" href="#carouselProduto" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
					</a>
				</div>
			</div>
			<div class="col-sm-7">
				<h1><?php echo $info['titulo']; ?></h1>
				<h4><?php echo $info['categorias']; ?></h4>
				<p><?php echo $info['descricao']; ?></p><br>
				<h3>R$: <?php echo number_format($info['valor'],2,',','.'); ?></h3><br>
				<p>
					<a class="btn btn-primary" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapseExample">
						Ver telefone
					</a>
				</p>
				<div class="collapse" id="collapse">
					<div class="card card-body">
						Telefone: <?php echo $info['telefone']; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php require 'pages/footer.php';?>