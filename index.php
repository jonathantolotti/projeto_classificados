<?php
require 'pages/header.php'; 
require 'classes/anuncios.class.php';
require 'classes/usuarios.class.php';
require 'classes/categorias.class.php';

$a = new Anuncios();
$u = new Usuarios();
$c = new Categorias();

$filtros = array(
	'categoria' => '',
	'preco1'    => '',
	'preco2'	=> '',
	'estado' 	=> ''
);
if(isset($_GET['filtros'])){
	$filtros = $_GET['filtros'];
}

$totalAnuncio = $a->getTotalAnuncios();
$totalUsuario = $u->getTotalUsuarios();
$p = 1;
if(isset($_GET['p']) && !empty($_GET['p'])){
	$p = addslashes($_GET['p']);
}
$perPage = 5;

$totalPaginas = ceil($totalAnuncio / $perPage);


$anuncios = $a->getUltimosAnuncios($p,$perPage, $filtros);
$categorias = $c->getCategorias();


?>
<!DOCTYPE html>
<html>
<body>
	<div class="container-fluid">
		<div class="jumbotron" style="margin-bottom: 5px; height: 200px;">
			<div class="container-fluid">
				<h3 class="display-4">Temos <?php echo $totalAnuncio; ?> anúncios publicados!</h3>
				<p class="lead">E mais de <?php echo $totalUsuario; ?> usuários cadastrados.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<h3>Pesquisa avançada:</h3>
			<form method="GET">
				<div class="form-group col-md-10">
					<label for="categoria">Categoria:</label>
					<select class="form-control" name="filtros[categoria]" id="categoria">
						<option></option>
						<?php foreach ($categorias as $cat): ?>
							<option value="<?php echo $cat['id']; ?>" <?php echo ($cat['id'] == $filtros['categoria'])?'selected="selected"':''; ?>><?php echo $cat['nome']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group col-md-4">
					<label for="valor1">Valor de:</label>
					<input type="text" class="form-control" name="filtros[preco1]" id="valor1">
				</div>

				<div class="form-group col-md-4">
					<label for="valor2">Valor até:</label>
					<input type="text" class="form-control" name="filtros[preco2]" id="valor2">
				</div>

				<div class="form-group col-md-10">
					<label for="estado">Estado:</label>
					<select class="form-control" name="filtros[estado]" id="estado">
						<option></option>
						<option value="NOVO" <?php echo ($filtros['estado'] == 'NOVO')?'selected="selected"':''; ?>>Novo</option>
						<option value="USADO" <?php echo ($filtros['estado'] == 'USADO')?'selected="selected"':''; ?>>Usado</option>
					</select>
				</div>

				
				<div class="form-group">
					<input class="btn btn-info" type="submit" value="Buscar">	
				</div>
			</form>		
		</div>
		<div class="col-sm-7">
			<h3>Últimos anúncios:</h3>
			<div class="table-responsive">
				<table class="table table-hover">
					<tbody>
						<?php foreach ($anuncios as $anuncio): ?>
							<tr>
								<td>
									<?php if(!empty($anuncio['url'])): ?>
										<img src="assets/_images/_anuncios/<?php echo $anuncio['url']; ?>" border="0" width="50px" height="50px">
										<?php else: ?>
											<img src="assets/_images/_anuncios/default.png" border="0" width="50px" height="50px" alt="Sem imagem disponível">
										<?php endif; ?>
									</td>
									<td>
										<a href="produto.php?id=<?php echo $anuncio['id']; ?>"><?php echo $anuncio['titulo'];?></a><br>
										<?php echo $anuncio['categoria']; ?>
									</td>
									<td>
										R$ <?php echo number_format($anuncio['valor'], 2, ',', '.'); ?>
									</td>
								</tr>

							<?php endforeach; ?>
						</tbody>
					</table>
					<ul class="pagination justify-content-center">
						<?php for($i=1;$i<=$totalPaginas;$i++): ?>
							<li class="page-item <?php echo($p == $i)?'active':''; ?>"><a class="page-link" href="index.php?<?php
							$w = $_GET;
							$w['p'] = $i;
							echo http_build_query($w);
							?>"><?php echo $i; ?></a></li>
						<?php endfor; ?>
					</ul>
				</div>
			</div>
		</div>
		<?php require 'pages/footer.php';?>